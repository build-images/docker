################################################################################
#°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
# Ubuntu avec docker-cli, git curl, emacs, vim
#°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
################################################################################
FROM ubuntu:16.04

MAINTAINER Sebastien Allamand "sebastien@allamand.com"

ENV DEBIAN_FRONTEND=noninteractive \
    INITRD=No \
    DEBCONF_NONINTERACTIVE_SEEN=true \
    DOCKER_VERSION=latest \
    DOCKER_COMPOSE_VERSION=1.15.0

LABEL org.label-schema.name="Docker"
LABEL org.label-schema.description="Docker daemon + docker client v${DOCKER_VERSION} ; Docker-compose v${DOCKER_COMPOSE_VERSION}"
LABEL org.label-schema.url="https://gitlab.com/build-images/docker"
LABEL org.label-schema.summary="Docker daemon (DinD mode) + docker client"
LABEL org.label-schema.version="${DOCKER_VERSION}"
LABEL org.label-schema.changelog-url="https://gitlab.com/build-images/docker/blob/master/CHANGELOG"
LABEL org.label-schema.vendor="Sébastien Allamand <sebastien@allamand.com>"
LABEL org.label-schema.schema_version="RC1"


RUN apt-get update -qq \
    && echo 'Installing OS dependencies' \
    && apt-get install -qq -y --fix-missing git ssh python curl ca-certificates locales tzdata \ 
               emacs-nox vim \
    #install compile tols
    && apt-get install -qq -y --fix-missing gcc \

    && locale-gen fr_FR.UTF-8 \
    && echo "Europe/Paris" > /etc/timezone && dpkg-reconfigure tzdata \

    && echo "Install Docker" \
    && cd /tmp \
    && curl -SLO https://get.docker.com/builds/Linux/x86_64/docker-latest.tgz \
    && tar zxvf docker-latest.tgz \
    && mv docker/docker /usr/bin/ \
    && docker --version \  

    && echo "Install Docker Compose" \
    && echo "debug: curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose" \
    && curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose \
    && docker-compose --version \


    && echo 'Cleaning up' \
    && apt-get clean -qq -y \
    && apt-get autoclean -qq -y \
    && apt-get autoremove -qq -y \
    && rm -rf /var/lib/apt/lists/* \
    rm -rf /tmp/*

#install docker-machine
RUN curl -L https://github.com/docker/machine/releases/download/v0.12.2/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine \
    && chmod +x /tmp/docker-machine \
    && cp /tmp/docker-machine /usr/local/bin/docker-machine \
    && rm -rf /tmp/*    

#Install Go 1.8
RUN curl -O https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz \
    && tar -xvf go1.8.linux-amd64.tar.gz \
    && mv go /usr/local \
    && rm go1.8.linux-amd64.tar.gz


ENV LANG=fr_FR.UTF-8 \
    LANGUAGE=fr_FR:fr \
    LC_ALL=fr_FR.UTF-8 \
    PATH=$PATH:/usr/local/go/bin


# Install pwd docker-machine plugin
RUN curl -L https://github.com/play-with-docker/docker-machine-driver-pwd/releases/download/v0.0.5/docker-machine-driver.tar -o /tmp/docker-machine-driver.tar \
    && tar -xf /tmp/docker-machine-driver.tar -C /tmp \
    && cp /tmp/linux/amd64/docker-machine-driver-pwd /usr/local/bin/ && chmod +X /usr/local/bin/docker-machine-driver-pwd \
    && rm -rf /tmp/*


#test
#RUN go version \
#    && docker version || true \
#    && docker-compose version \
#    && docker-machine version || true \
#    && docker-machine create -d pwd --pwd-url http://host1.labs.play-with-docker.com/p/f47321e1-8a0b-4ee1-ae1d-43d0e1a56baa node3
    

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["bash"]
